package br.com.pucsp.yoloteam.facedemo

import org.opencv.core.*
import org.opencv.face.FaceRecognizer
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.opencv.imgcodecs.Imgcodecs
import javax.swing.Spring.height
import javax.swing.Spring.width
import org.opencv.imgproc.Imgproc
import org.opencv.objdetect.CascadeClassifier
import org.springframework.util.ResourceUtils

@SpringBootApplication
class FaceDemoApplication

fun main(args: Array<String>) {
	//runApplication<FaceDemoApplication>(*args)
	nu.pattern.OpenCV.loadShared()
	//System.loadLibrary(Core.NATIVE_LIBRARY_NAME)
	val mat = Mat.eye(3, 3, CvType.CV_8UC1)
	println("mat = " + mat.dump())
	DetectFaceDemo().run()
}

internal class DetectFaceDemo {
	fun run() {
		println("\nRunning DetectFaceDemo")
		// Create a face detector from the cascade file in the resources
		// directory.
		//val faceDetector = CascadeClassifier(javaClass.getResource("/lbpcascade_frontalface.xml").path)
		val faceDetector = CascadeClassifier(ResourceUtils.getFile("classpath:haarcascade_frontalcatface_extended.xml").path)
//		val image = Imgcodecs.imread(javaClass.getResource("/lena.png").path)
		val image = Imgcodecs.imread( ResourceUtils.getFile("classpath:cat.jpeg").path)
		// Detect faces in the image.
		// MatOfRect is a special container class for Rect.
		val faceDetections = MatOfRect()
		faceDetector.detectMultiScale(image, faceDetections)
		println(String.format("Detected %s faces", faceDetections.toArray().size))
		// Draw a bounding box around each face.
		for (rect in faceDetections.toArray()) {
			Imgproc.rectangle(image, Point(rect.x.toDouble(), rect.y.toDouble()), Point(rect.x.toDouble() + rect.width, rect.y .toDouble()+ rect.height), Scalar(0.0, 255.0, 0.0))
		}
		// Save the visualized detection.
		val filename = "faceDetection.png"
		println(String.format("Writing %s", filename))
		Imgcodecs.imwrite(filename, image)
		val x : FaceRecognizer
	}


}


